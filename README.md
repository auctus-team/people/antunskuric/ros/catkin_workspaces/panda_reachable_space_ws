## Panda ROS workspace for reachable space evaluation reachable space evaluation 
This is an example catkin workspace for panda reachable space calculation. In the this directory you can find the implementation of the python reachable space package for the Panda robot. The directory consists of two ros packages:
- franka_description: Panda robot definitions from Franka Emika  http://wiki.ros.org/franka_description
- **panda_reachable_space: the reachable space solver for Panda robot**


<img src="src/panda_reachable_space/images/output1.gif" height="200px"/><img src="src/panda_reachable_space/images/output11.gif"  height="200px"/><img src="src/panda_reachable_space/images/output2.gif"  height="200px"/>


### Install the ros packages - using catkin
To run panda robot capacity calculation nodes first clone the repository and submodules:
```shell
git clone https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/catkin_workspaces/panda_reachable_space_ws.git
cd panda_reachable_space_ws
```
And then just build the workspace
```shell
cd ..
catkin build
```
And you should be ready to go!

# panda_reachable_space package

`panda_reachable_space` package is an example implementation of the task space capacity calculation for for Franka Emika Panda robot in a form of a catkin ros package.

It uses the library [pinocchio](https://github.com/stack-of-tasks/pinocchio) for calculating robot kinematics.
## Dependencies
For visualizing the polytopes in RVIZ you will need to install the [jsk-rviz-plugin](https://github.com/jsk-ros-pkg/jsk_visualization)

```sh
sudo apt install ros-*-jsk-rviz-plugins # melodic/kinetic... your ros version
```

The code additionally depends on the pinocchio library. 

### Pinocchio already installed
If you do already have pinocchio installed simply install the the pip package `pycapacity`
```
pip install pycapacity
```
And you're ready to go!

### Installing pinocchio

If you dont have pinocchio installed we suggest you to use Anaconda. Create the environment will all dependencies:

```bash
conda env create -f env.yaml 
conda activate panda_reachable_space  # activate the environment
```

Once the environment is activated you are ready to go.


### One panda simulation
Once when you have everything installed you will be able to run the interactive simulations and see the polytope being visualised in real-time.


<img src="src/panda_reachable_space/images/output_simple1.gif" height="400px"/>

To see a simulation with one panda robot and its force and velocity manipulatibility ellipsoid and polytope run the command in the terminal.

```shell
source ~/capacity_ws/devel/setup.bash 
roslaunch panda_reachable_space one_panda.launch
```

> If using anaconda, don't forget to call <br>
> `conda activate panda_reachable_space` <br> 
> before you call `roslaunch`