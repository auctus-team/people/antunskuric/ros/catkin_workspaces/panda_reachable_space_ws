# panda_reachable_space package

`panda_reachable_space` package is an example implementation of the reachable space approximation for for Franka Emika Panda robot in a form of a catkin ros package.

It uses the library [pinocchio](https://github.com/stack-of-tasks/pinocchio) for calculating robot kinematics.
## Dependencies
For visualizing the polytopes in RVIZ you will need to install the [jsk-rviz-plugin](https://github.com/jsk-ros-pkg/jsk_visualization)

```sh
sudo apt install ros-*-jsk-rviz-plugins # melodic/kinetic... your ros version
```

The code additionally depends on the pinocchio library. 

### Pinocchio already installed
If you do already have pinocchio installed simply install the the pip package `pycapacity`
```
pip install pycapacity
```
And you're ready to go!

### Installing pinocchio

If you dont have pinocchio installed we suggest you to use Anaconda. Create the environment will all dependencies:

```bash
conda env create -f env.yaml 
conda activate panda_reachable_space  # activate the environment
```

Once the environment is activated you are ready to go.



### One panda simulation
Once when you have everything installed you will be able to run the interactive simulations and see the polytope being visualised in real-time.

<img src="images/output_simple1.gif" height="300px">

To see a simulation with one panda robot and its reachable space polytopes run the command in the terminal.

```shell
source ~/capacity_ws/devel/setup.bash 
roslaunch panda_reachable_space one_panda.launch
```
> If using anaconda, don't forget to call <br>
> `conda activate panda_reachable_space` <br> 
> before you call `roslaunch`


You can also visualise the reachable space of the full robot by enabling checkboxes `full_rs_lines` and `full_rs_faces`

<img src="images/output_simple.gif" height="300px">
