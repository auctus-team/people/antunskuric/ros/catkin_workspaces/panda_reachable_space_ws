#!/usr/bin/env python
from turtle import position
import rospkg
import os
import numpy as np

# polytope python module
import pycapacity.robot as capacity_solver
import pycapacity.algorithms as algos

# URDF parsing an kinematics 
import pinocchio as pin


# --------------------------------------------------------------
import numpy as np
import itertools
from scipy.spatial import ConvexHull, HalfspaceIntersection
from cvxopt import matrix
import cvxopt.glpk

from pycapacity.algorithms import stack
# --------------------------------------------------------------    

class RobotSolver:

    # constructor - reading urdf and constructing the robots kinematic chain
    def __init__(self, base, tip):
        rospack = rospkg.RosPack()
        path = rospack.get_path('panda_reachable_space')
        # urdf_path = os.path.join(path,"scripts","capacity","panda.urdf")
        urdf_path = os.path.join(path,"scripts","capacity","panda_link_center.urdf")

        # loading the root urdf from robot_description parameter
        self.robot = pin.buildModelFromUrdf(urdf_path)

        self.base_link = base
        self.tip_link = tip
        self.base_id = self.robot.getFrameId(base)
        self.tip_id = self.robot.getFrameId(tip)


        # https://frankaemika.github.io/docs/control_parameters.html
        # maximal torques
        self.t_max = self.robot.effortLimit.T
        self.t_min = -self.t_max
        # maximal joint velocities
        self.dq_max = self.robot.velocityLimit.T
        self.dq_min = -self.dq_max
        # maximal joint angles
        self.q_max = self.robot.upperPositionLimit.T
        self.q_min = self.robot.lowerPositionLimit.T

        self.ddq_max = np.array([15,7.5,10,12.5,15,20,20]).T       
        self.ddq_min = -self.ddq_max

        self.dddq_max = np.array([7500.0,3750.0,5000.0,6250.0,7500.0,10000.0,10000]).T       
        self.dddq_min = -self.dddq_max


        self.ddx_max = np.array([13.0,13,13])
        self.ddx_min = -self.ddx_max
        self.dx_max = np.array([1.7,1.7,1.7])
        self.dx_min = -self.dx_max


        self.data = self.robot.createData()

    # direct kinematics functions for 7dof
    def forward(self, q, frame_name=None):
        frame_id = self.tip_id
        if frame_name is not None:
            frame_id = self.robot.getFrameId(frame_name)
        pin.framesForwardKinematics(self.robot,self.data, np.array(q))
        return self.data.oMf[frame_id]
        
    def dk_position(self, q, frame_name=None):
        return self.forward(q, frame_name).translation

    def dk_orientation_matrix(self, q, frame_name=None):
        return self.forward(q,frame_name).rotation

    def jacobian(self, q, frame_name=None):
        frame_id = self.tip_id
        if frame_name is not None:
            frame_id = self.robot.getFrameId(frame_name)
        pin.computeJointJacobians(self.robot,self.data, np.array(q))
        Jac = pin.getFrameJacobian(self.robot, self.data, frame_id, pin.LOCAL_WORLD_ALIGNED)
        return np.matrix(Jac)

    def jacobian_position(self, q,frame_name=None):
        return self.jacobian(q, frame_name)[:3, :]

    def jacobian_pseudo_inv(self, q):
        return np.linalg.pinv(self.jacobian(q))

    def gravity_torque(self, q):
        pin.computeGeneralizedGravity(self.robot,self.data, np.array(q))
        return np.array(self.data.g)

    def mass_matrix(self, q):
        pin.crba(self.robot,self.data,np.array(q))
        return np.matrix(self.data.M)


    # maximal end effector force
    def reachable_space_polytope(self, q, dela_t, m_o, direction = np.eye(3,6), D = None, k = None):

        # jacobian calculation
        Jac_full = self.jacobian(q)
        if direction is not None:
            Jac = np.array(direction).dot(Jac_full)
        else:
            Jac = Jac_full

        m,n = Jac.shape
        
        M = self.mass_matrix(q)
        M_inv = np.linalg.pinv(M)

        t_bias = self.gravity_torque(q) + np.array(Jac.T.dot([0,0,9.81*m_o])).flatten()

        dq = np.array(np.linalg.pinv(Jac).dot(np.array([0,0,0])))

        u, s, v = np.linalg.svd(Jac)
        V2 = np.array(v.transpose()[:,len(s):])

        A_inv = Jac.dot(M_inv).dot(Jac.T)

        # D.x < k
        if np.any(D) and np.any(k):
            De = D.dot(Jac.dot(M_inv)*dela_t**2/2)
            ke = k - D.dot(self.dk_position(q))

            vertex, H,d, faces_index,t_vert,x_vert =  algos.iterative_convex_hull_method(
                A = np.eye(3),
                B = Jac.dot(M_inv)*dela_t**2/2,
                y_max= self.t_max - t_bias, 
                y_min = self.t_min - t_bias,
                G_in = np.vstack((
                    M_inv*dela_t**2/2,
                    -M_inv*dela_t**2/2, 
                    M_inv*dela_t, 
                    -M_inv*dela_t, 
                    De,
                    )),
                h_in = np.hstack((
                    (self.q_max -q).flatten(), 
                    (q - self.q_min).flatten(),
                    (self.dq_max - dq).flatten(), 
                    (dq-self.dq_min).flatten(),
                    ke,
                    )),
                tol = 0.0001)
        else:
            vertex, H,d, faces_index,t_vert,x_vert =  algos.iterative_convex_hull_method(
                A = np.eye(3),
                B = Jac.dot(M_inv)*dela_t**2/2,
                y_max= self.t_max - t_bias, 
                y_min = self.t_min - t_bias,
                G_in = np.vstack((
                    M_inv*dela_t**2/2,
                    -M_inv*dela_t**2/2, 
                    M_inv*dela_t, 
                    -M_inv*dela_t, 
                    )),
                h_in = np.hstack((
                    (self.q_max -q).flatten(), 
                    (q - self.q_min).flatten(),
                    (self.dq_max - dq).flatten(), 
                    (dq-self.dq_min).flatten(),
                    )),
                tol = 0.0001)



        faces = capacity_solver.face_index_to_vertex(vertex,faces_index)
        return vertex, faces



    # maximal end effector force
    def reachable_space_polytope_full(self, q, dela_t, m_o, direction = np.eye(3,6), D = None, k = None, options=None):

        # jacobian calculation
        Jac_full = self.jacobian(q)
        if direction is not None:
            Jac = np.array(direction).dot(Jac_full)
        else:
            Jac = Jac_full

        m,n = Jac.shape

        M_inv = np.linalg.pinv(self.mass_matrix(q))
    
        t_bias = self.gravity_torque(q) + np.array(Jac.T.dot([0,0,9.81*m_o])).flatten()

        frames = ['panda_link2','panda_link4_center','panda_link4', 'panda_link6_center', 'panda_link8']
        link_occupacny_limits = [
            [[-0.15,0,-0.15],[0.15,0,0.15]],
            [[-0.07,-0.12,0],[0.15,0.12,0]],
            [[-0.15,0,-0.15],[0.05,0,0.1]],
            [[-0.05,0,-0.1],[0.15,0,0.15]],
            [[-0.1,-0.1,-0.25],[0.1,0.1,0.1]],
        ]

        if options is not None and "extend_links" in options and options['extend_links'] : 
            extend_links = True
        else:
            extend_links = False

        if options is not None and "one_polytope" in options and options['one_polytope'] : 
            one_polytope = True
        else:
            one_polytope = False
            
        if options is not None and "link_polytope" in options and options['link_polytope'] : 
            link_polytope = True
        else:
            link_polytope = False

        verts = np.array([])
        faces = np.array([])
        verts_list = []
        for i,frame_name in enumerate(frames):

            # jacobian calculation
            Jac = self.jacobian(q,frame_name)
            Jac = np.array(direction).dot(Jac)

            R = self.dk_orientation_matrix(q,frame_name)

            # optimistation problem definition
            if not extend_links: 
                M_ineq = M_inv
                B_ = Jac.dot(M_inv)*dela_t**2/2
                y_min_ = self.t_min - t_bias
                y_max_ = self.t_max  - t_bias
            else:
                x_size_max = np.array(link_occupacny_limits[i][1])
                x_size_min = np.array(link_occupacny_limits[i][0])
                M_ineq = np.hstack((M_inv, np.zeros((M_inv.shape[0], 3))))
                B_ = np.hstack((Jac.dot(M_inv)*dela_t**2/2, R))
                y_min_ = np.hstack((self.t_min - t_bias, x_size_min))
                y_max_ = np.hstack((self.t_max - t_bias, x_size_max))
                

            G_in_ = np.vstack((
                    M_ineq*dela_t**2/2,
                    -M_ineq*dela_t**2/2, 
                    M_ineq*dela_t, 
                    -M_ineq*dela_t, 
                    ))
            
            h_in_ = np.hstack((
                    (self.q_max -q).flatten(), 
                    (q - self.q_min).flatten(),
                    self.dq_max, 
                    -self.dq_min,
                    ))

            # D.x < k
            if np.any(D) and np.any(k):
                De = D.dot(Jac.dot(M_inv)*dela_t**2/2)
                ke = k - D.dot(self.dk_position(q,frame_name))
                if extend_links:  # if extended case
                    De = np.hstack((De, np.zeros((De.shape[0], 3))))
                G_in_ = np.vstack((G_in_,De))
                h_in_ = np.hstack((h_in_,ke))

            
            vertex, H,d, faces_index,t_vert,x_vert =  algos.iterative_convex_hull_method(
                A = np.eye(3),
                B = B_,
                y_max = y_max_,
                y_min = y_min_,
                G_in = G_in_,
                h_in = h_in_,
                tol = 0.0001)

            if verts.size != 0:
                vertex = vertex + self.dk_position(q, frame_name).reshape((-1,1)) - self.dk_position(q).reshape((-1,1))
                verts_list.append(vertex)
                verts = np.hstack((verts,vertex))
                if not one_polytope:
                    faces = np.vstack((faces,capacity_solver.face_index_to_vertex(vertex,faces_index)))
            else:
                vertex = vertex + self.dk_position(q, frame_name).reshape((-1,1)) - self.dk_position(q).reshape((-1,1))
                verts_list.append(vertex)
                verts = vertex
                if not one_polytope:
                    faces = capacity_solver.face_index_to_vertex(vertex,faces_index)


        if link_polytope:
            v = np.hstack((verts_list[0],verts_list[1]))
            faces_ind = algos.vertex_to_faces(v)
            faces = capacity_solver.face_index_to_vertex(v,faces_ind)

            v = np.hstack((verts_list[2],verts_list[3]))
            faces_ind = algos.vertex_to_faces(v)
            faces = np.vstack((faces,capacity_solver.face_index_to_vertex(v,faces_ind)))

            v = verts_list[4]
            faces_ind = algos.vertex_to_faces(v)
            faces = np.vstack((faces,capacity_solver.face_index_to_vertex(v,faces_ind)))

        if extend_links:
            vertex_base = np.array([[0.1,0.15,0],[0.1,-0.15,0],[-0.15,0.15,0],[-0.15,-0.15,0.0],[0.1,0.15,0.35],[0.1,-0.15,0.35],[-0.15,0.15,0.35],[-0.15,-0.15,0.35]]).T  - self.dk_position(q).reshape((-1,1))
        else:
            vertex_base = np.array([[0.1,0.15,0],[0.1,-0.15,0],[-0.15,0.15,0],[-0.15,-0.15,0.0]]).T  - self.dk_position(q).reshape((-1,1))
        
        verts = np.hstack((verts,vertex_base))
        if not one_polytope:
            faces_base = algos.vertex_to_faces(vertex_base)
            faces_base = capacity_solver.face_index_to_vertex(vertex_base,faces_base)
            faces = np.vstack((faces, faces_base))
        else:
            faces = algos.vertex_to_faces(verts)
            faces = capacity_solver.face_index_to_vertex(verts,faces)

        return verts, faces
    
      


    def reachable_space_polytopeX(self, q, delta_t, m_o, direction = np.eye(3,6)):


        vertex, H,d, faces_index, t_vert, x_vert =  algos.iterative_convex_hull_method(
            A = np.eye(3),
            B = np.eye(3)*delta_t**2/2,
            y_max = self.ddx_max, 
            y_min = self.ddx_min,
            G_in = np.vstack((np.eye(3)*delta_t, -np.eye(3)*delta_t)),
            h_in = np.hstack((self.dx_max, -self.dx_min)),
            tol = 0.001,
            max_iter=500)

        faces = capacity_solver.face_index_to_vertex(vertex,faces_index)
        return vertex, faces