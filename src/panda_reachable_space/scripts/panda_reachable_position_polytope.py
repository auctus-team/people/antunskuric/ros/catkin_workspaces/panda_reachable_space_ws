#!/usr/bin/env python
import rospy
# time evaluation
import time
#some math
import numpy as np
# polygone messages
from sensor_msgs.msg import JointState, PointCloud 
from jsk_recognition_msgs.msg import PolygonArray
from std_msgs.msg import Float64

# robot module import
from capacity.robot_solver import RobotSolver 
# capacity visualisation utils
import capacity.capacity_visual_utils as capacity_utils

# getting the node namespace
namespace = rospy.get_namespace()

# instance of robot solver
panda = RobotSolver("panda_link0", "panda_link8")
frame = namespace[1:]  + panda.base_link

# initial joint positions
joint_positions = [0,0,0,0,0,0,0]
joint_velocity = [0,0,0,0,0,0,0]
joint_states_timestamp = time.time()



import dynamic_reconfigure.client
from panda_reachable_space.cfg import HorizonConfig
from dynamic_reconfigure.server import Server

delta_t = 0.2
def callback_server(config, level):
    global delta_t, options
    rospy.loginfo("""Reconfigure Request: time horizon: {delta_t} sec""".format(**config))
    delta_t = config['delta_t']
    return config

# function receiveing the new joint positions
def callback(data):
    global joint_positions, joint_velocity, joint_states_timestamp
    now_timestamp = time.time()
    joint_velocity = (np.array(data.position) - joint_positions)/(now_timestamp - joint_states_timestamp)
    joint_positions = data.position
    joint_states_timestamp = now_timestamp

def plot_polytope(q,q_dot):
    
    scaling_factor = 1

    if not any(q):
        return

    # calculate dk of the robot
    # just for visualisation - to display the ellipsoid on the tip of the robot
    pose = panda.dk_position(q)
   
    # calculate reachable_position vertexes
    start = time.time()
    # A = np.array([[0,0,-1],
    #             [0,-1,0]])
    # b = np.array([-0.51,0.1])
    # no environment
    A = None
    b = None
    reachable_position_vertex, reachable_position_polytopes = panda.reachable_space_polytope(q, dela_t=delta_t, m_o=0, D=A, k=b)
    print(time.time() - start)
        
    # publish vertices
    publish_reachable_position_polytop_vertex = rospy.Publisher(namespace+'reachable_position_polytope_vertex', PointCloud, queue_size=10)
    publish_reachable_position_polytop_vertex.publish(capacity_utils.create_vertex_msg(reachable_position_vertex, pose, frame, scaling_factor))

    # publish plytope
    publish_reachable_position_polytope = rospy.Publisher(namespace+'reachable_position_polytope', PolygonArray, queue_size=10)
    publish_reachable_position_polytope.publish(capacity_utils.create_polytopes_msg(reachable_position_polytopes, pose, frame, scaling_factor))
    

# main function of the class
def panda_polytope():
    global joint_positions
    rospy.init_node('panda_polytope')

    joint_state_topic = rospy.get_param('joint_state_topic', 'joint_states')
    rospy.Subscriber(namespace + joint_state_topic, JointState, callback, queue_size=1)


    # global variables
    server = Server(HorizonConfig, callback_server)
    client = dynamic_reconfigure.client.Client(namespace + "panda_reachable_position_polytope", timeout=10, config_callback=None)
    client.update_configuration({"delta_t":delta_t})

    rate = rospy.Rate(50) #Hz
    while not rospy.is_shutdown():
        plot_polytope(joint_positions,joint_velocity)
        rate.sleep()

# class definition
if __name__ == '__main__':
    try:
        panda_polytope()
    except rospy.ROSInterruptException:
        pass